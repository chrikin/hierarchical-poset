package org.ottr.menubar.models;

import org.ottr.menubar.controllers.ModeMenuActionListener;

import java.io.Serializable;

/**
 * Holds state information for the menu.
 */
public class MenuMode implements Serializable
{
    private mode chosenMode;

    /**
     * Class constructor. Sets the mode as PICKING as that is the default mode when the user starts the application.
     * @see ModeMenuActionListener#selectModePicking()
     */
    public MenuMode()
    {
        chosenMode = mode.PICKING;
    }

    /**
     * Getter for the current mode of the application, usually done to check mode-specific features, like the preview
     * box in preview mode.
     * @return mode of the application
     */
    public mode getChosenMode()
    {
        return chosenMode;
    }

    /**
     * Setter for the mode to picking.
     * @see ModeMenuActionListener#selectModePicking()
     */
    public void setPickingMode()
    {
        chosenMode = mode.PICKING;
    }

    /**
     * Setter for the mode to transforming.
     * @see ModeMenuActionListener#selectModeTransforming()
     */
    public void setTransformingMode()
    {
        chosenMode = mode.TRANSFORMING;
    }

    /**
     * Setter for the mode to previewing.
     * @see ModeMenuActionListener#selectModePreviewing()
     */
    public void setPreviewingMode()
    {
        chosenMode = mode.PREVIEWING;
    }

    /**
     * Represents the mode.
     * @see ModeMenuActionListener#selectModePicking()
     * @see ModeMenuActionListener#selectModeTransforming()
     * @see ModeMenuActionListener#selectModePreviewing()
     */
    public enum mode
    {
        PICKING,
        TRANSFORMING,
        PREVIEWING
    }
}
