package org.ottr.graph.models.representation;

import java.io.Serializable;

/**
 * Represents a directed edge between two vertices with a start and destination.
 */
public class Edge implements Serializable
{
    private NestedGraphTree startVertex;
    private NestedGraphTree destinationVertex;

    /**
     * Class constructor
     * @param startVertex vertex edge originates from
     * @param destinationVertex vertex edge ends at
     */
    public Edge
            (
                    NestedGraphTree startVertex,
                    NestedGraphTree destinationVertex
            )
    {
        this.startVertex = startVertex;
        this.destinationVertex = destinationVertex;
    }

    /**
     * Getter for the vertex that the edge originates from.
     * @return edge start vertex
     */
    public NestedGraphTree getStartVertex()
    {
        return startVertex;
    }

    /**
     * Setter for the vertex that the edge originates from.
     * @param newStartVertex new vertex of edge start
     */
    public void setStartVertex(NestedGraphTree newStartVertex)
    {
        startVertex = newStartVertex;
    }

    /**
     * Getter for the vertex that the edge ends at.
     * @return edge end vertex
     */
    public NestedGraphTree getDestinationVertex()
    {
        return destinationVertex;
    }

    /**
     * Setter for the vertex that the edge ends at.
     * @param newDestinationVertex new vertex of edge end
     */
    public void setDestinationVertex(NestedGraphTree newDestinationVertex)
    {
        destinationVertex = newDestinationVertex;
    }
}
