package org.ottr.graph.models.representation;

import org.json.simple.JSONObject;
import org.ottr.graph.controllers.listeners.NestedGraphButtonListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

/**
 * Represents the information object of a vertex, i.e. its additional information.
 */
public class InformationObject implements Serializable
{
    private final JSONObject rawObject;
    private final boolean isMerged;

    /**
     * Class constructor for creating an information object out of a JSON object of key-value pairs.
     * @param rawObject the raw JSON object to extract information from
     */
    // Information object is encoded in a JSON object
    public InformationObject(JSONObject rawObject)
    {
        this.rawObject = rawObject;
        isMerged = false;
    }

    /**
     * Class constructor for creating an information object out of a list of JSON object, usually done after a merge,
     * in which the new merged vertex's information object will need to be constructed from the information objects
     * of the vertices that were merged to create it.
     * @param mergedInfoObjects the list of merged vertices
     * @param mergedVertexIds the list of merged vertex id's
     * @see NestedGraphButtonListener#merge()
     */
    // When merging a vertex must have all info objects of merged vertices
    public InformationObject
    (
            ArrayList<InformationObject> mergedInfoObjects,
            ArrayList<Integer> mergedVertexIds
    )
    {
        rawObject = new JSONObject();
        isMerged = true;

        for(int i = 0; i < mergedInfoObjects.size(); i++)
        {
            rawObject.put(mergedVertexIds.get(i),mergedInfoObjects.get(i));
        }
    }

    /**
     * Creates a string representation of a given information object, usually used to be shown as the additional
     * information in the {@link org.ottr.graph.views.RightSideScreen}.
     * @return a string representation of the object
     */
    public String getInfoObjectAsString()
    {
        StringBuilder formatted_json_string = new StringBuilder();

        for(Object curr_key : rawObject.keySet())
        {
            Object curr_value = rawObject.get(curr_key);

            // If the keys are strings, just append them - it means that this vertex isn't a merged vertex
            if(curr_value instanceof String)
            {
                formatted_json_string
                        .append("<p>&emsp;<b>")
                        .append(curr_key)
                        .append("</b>: ")
                        .append(curr_value)
                        .append("</p>");
            }
            // If the keys are JSON objects themselves, call the method recursively until you get strings to append
            else if(curr_value instanceof InformationObject)
            {
                formatted_json_string
                        .append("<p><b>Vertex ")
                        .append(curr_key)
                        .append("</b>: </p>");
                formatted_json_string // recursive call happens here!
                        .append(((InformationObject) curr_value).getInfoObjectAsString());
            }
        }
        return formatted_json_string.toString();
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        InformationObject that = (InformationObject) o;
        return isMerged == that.isMerged && rawObject.equals(that.rawObject);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(rawObject, isMerged);
    }
}
