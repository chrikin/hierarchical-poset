package org.ottr.graph.views;

import com.google.common.base.Function;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.decorators.EdgeShape;
import edu.uci.ics.jung.visualization.renderers.Renderer;
import org.ottr.graph.models.representation.NestedGraphTree;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;

/**
 * Sets properties for vertices and edges like colour, fonts, labels, shapes, sizes, etc.
 */
public class VisualisationViewerTransformers
{
    /**
     * Specifies properties for vertices and edges like colour, fonts, labels, shapes, sizes, etc. given a visualisation
     * viewer and the current graph it's supposed to add the properties to.
     * @param vv the visualisation viewer to add properties to
     * @param currentGraph the graph from which information about vertex/edge id's is taken to specify specific
     *                     properties to specified vertices/edges
     * @param mergedVertices the list of merged vertices, from which merged vertices information is taken, since they
     *                       are often coloured in a different way; at the beginning merged vertices will be an empty
     *                       collection, so the first time this method is called this parameter will be redundant
     */
    public static void addTransformers
            (
                    VisualizationViewer<Integer, String> vv,
                    NestedGraphTree currentGraph,
                    ArrayList<NestedGraphTree> mergedVertices
            )
    {

        Graph<Integer, String> graph = currentGraph.createGraph();

        // VERTEX TRANSFORMERS //

        // Vertex Label Transformer
        // Here we set the vertex label, if it's non-merged, the label is the vertex id, but if it is
        // merged, the label would be an upper-case "M" concatenated with the next available merge vertex id.
        // (Ex. The first ever merged vertex id will be "M1")
        vv.getRenderContext().setVertexLabelTransformer(new Function<Integer, String>()
        {
            @Override
            public String apply(Integer integer)
            {
                boolean is_selected_vertex_merged = currentGraph.getChildWithId(integer).isMerged();
                if(is_selected_vertex_merged)
                {
                    Integer mergedVertexPosition = -1;
                    for(int i = 0; i < mergedVertices.size() ; i++)
                    {
                        if(integer == mergedVertices.get(i).getId())
                        {
                            mergedVertexPosition = i + 1;
                        }
                    }
                    return "M" + mergedVertexPosition.toString();
                }
                else
                {
                    return integer.toString();
                }
            }
        });

        // Vertex Label Font Transformer
        // Here we set the font of the vertex labels, which is the same for all vertices.
        vv.getRenderContext().setVertexFontTransformer(new Function<Integer, Font>()
        {
            @Override
            public Font apply(Integer integer)
            {
                return new Font("SansSerif", Font.BOLD, 17);
            }
        });

        // Vertex Label Position
        // Here we set the position of the vertex label relative the vertex, it being central.
        vv.getRenderer().getVertexLabelRenderer().setPosition(Renderer.VertexLabel.Position.CNTR);

        // Vertex Shape and Size Transformer
        // Here we set the size and shape of the vertex. Currently, they're all circles, but deep vertices (so
        // ones that hold nested graphs within themselves) appear bigger, while shallow vertices - smaller. It's done
        // this way as a way to easily distinguish between them.
        vv.getRenderContext().setVertexShapeTransformer(new Function<Integer, Shape>()
        {
            @Override
            public Shape apply(Integer vertex_id)
            {
                // If vertex deep it's a bit bigger
                boolean is_selected_vertex_deep = currentGraph.getChildWithId(vertex_id).isVertexDeep();
                if(is_selected_vertex_deep)
                {
                    return new Ellipse2D.Double(-15,-15,40,40);
                }

                return new Ellipse2D.Double(-15,-15,30,30);
            }
        });

        // Vertex Colour Transformer
        // Here we set the colour of the vertices, it's important to note that this is the initial colour transformer!
        // Its job is to colour vertices *before* any events have taken place, there is a secondary transformer in
        // NestedGraphVertexSelectionListener that takes care of what should happen if vertices are selected/unselected
        // and if they're deep/shallow vertices.
        vv.getRenderContext().setVertexFillPaintTransformer(new Function<Integer, Paint>()
        {
            @Override
            public Paint apply(Integer vertex_id)
            {

                boolean is_selected_vertex_merged = currentGraph.getChildWithId(vertex_id).isMerged();
                // If MERGED, colour with blue
                if(is_selected_vertex_merged)
                {
                    return Color.decode("#0000FF");
                }
                // If NON-MERGED, colour with red
                else
                {
                    // If vertex deep it's a darker red
                    boolean is_selected_vertex_deep = currentGraph.getChildWithId(vertex_id).isVertexDeep();
                    if(is_selected_vertex_deep)
                    {
                        return Color.decode("#ff1d18");
                    }
                    // If vertex not deep it's lighter red
                    return Color.decode("#ff6865");
                }
            }
        });


        // EDGE TRANSFORMERS //

        // Edge Label Transformer
        // Here we set the edge label, which is currently empty
        vv.getRenderContext().setEdgeLabelTransformer(new Function<String, String>()
        {
            @Override
            public String apply(String s) {
                return "";
            }
        });

        // Edge Shape Transformer
        // Here we set the type of edge, which is currently a straight line
        EdgeShape<Integer,String> straight_line_transformer = new EdgeShape<Integer,String>(graph);
        vv.getRenderContext().setEdgeShapeTransformer(EdgeShape.line(graph));
    }
}
