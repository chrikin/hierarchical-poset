package org.ottr.graph.views;

import edu.uci.ics.jung.algorithms.layout.ISOMLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import org.ottr.App;
import org.ottr.menubar.views.GraphMenu;
import org.ottr.transformations.views.UndoTransformationDifferentViewerJDialog;
import org.ottr.transformations.views.UndoTransformationMergeChildrenJDialog;
import org.ottr.graph.models.representation.NestedGraphTree;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Holds all view elements of the application.
 */
public class MainView
{
    private final JFrame frame;
    private final JPanel graphScreen;
    private VisualizationViewer<Integer, String> visualizationViewer;
    private final RightSideScreen rightSideScreen;
    private final GraphMenu graphMenu;
    private final JFileChooser openFileBrowser;
    private final JFileChooser saveAsFileBrowser;
    private final UndoTransformationDifferentViewerJDialog undoTransformationDifferentViewerJDialog;
    private final UndoTransformationMergeChildrenJDialog undoTransformationMergeChildrenJDialog;

    /**
     * Class constructor.
     */
    public MainView()
    {
        // Elements from the main window
        frame = new JFrame("Graph Visualiser");
        graphScreen = new JPanel();
        rightSideScreen = new RightSideScreen();
        graphMenu = new GraphMenu();

        // File browsers for opening/saving state
        openFileBrowser = new JFileChooser(App.getSavePath());
        saveAsFileBrowser = new JFileChooser(App.getSavePath());

        // Dialog boxes edge cases for undoing transformations
        undoTransformationDifferentViewerJDialog = new UndoTransformationDifferentViewerJDialog(frame);
        undoTransformationMergeChildrenJDialog = new UndoTransformationMergeChildrenJDialog(frame);

        // We set the go up, go down, and merge button as not visible, as in the beginning there will be no option
        // to do any of these actions, more specifically:
        // To go down a hierarchical level or to merge vertices a vertex must be selected, in the beginning no vertex is
        //      selected yet.
        // To go up from a hierarchical level, the user must have already gone down a level, in the beginning the
        //      current graph is the root one, and no graph is above the root, i.e. the root has no parent
        rightSideScreen.getGoUpButton().setVisible(false);
        rightSideScreen.getGoDownButton().setVisible(false);
        rightSideScreen.getMergeButton().setVisible(false);
    }

    public JFrame getFrame()
    {
        return frame;
    }

    /**
     * Getter for the visualisation viewer, usually to change the graph it represents.
     * The reasons this isn't done with a designated visualisation viewer setter is that the visualisation viewer has
     * listeners on user input events so instead of changing the visualisation viewer each
     * time and setting new listeners, we keep the same visualisation viewer but change the graph it's showing.
     * @return visualisation viewer panel
     */
    public VisualizationViewer<Integer, String> getVisualizationViewer()
    {
        return visualizationViewer;
    }

    /**
     * Getter for the right side screen of the GUI, usually done to refer to its contents.
     * @return right side screen panel
     */
    public RightSideScreen getRightSideScreen()
    {
        return rightSideScreen;
    }

    /**
     * Getter for the menu bar of the GUI, usually done to refer to its contents.
     * @return menu object
     */
    public GraphMenu getGraphMenu()
    {
        return graphMenu;
    }

    /**
     * Getter for the file browser when opening a new file, usually to do with the open file menu option.
     * @return file browser for opening a new file
     * @see org.ottr.menubar.controllers.FileMenuActionListener
     */
    public JFileChooser getOpenFileBrowser()
    {
        return openFileBrowser;
    }

    /**
     * Getter for the file browser when saving a file, usually to do with the save/save as file menu option.
     * @return file browser for saving to a file
     * @see org.ottr.menubar.controllers.FileMenuActionListener
     */
    public JFileChooser getSaveAsFileBrowser()
    {
        return saveAsFileBrowser;
    }

    /**
     * Getter for the dialog box that is triggered when a user attempts to undo a transformation while not being on
     * the same screen as that transformation (in other words, the visualisation viewer that the user is on and the
     * one they're trying to change are different).
     * @return dialog box for when the user tries to undo a transformation, but aren't on the same screen
     * @see UndoTransformationDifferentViewerJDialog
     */
    public UndoTransformationDifferentViewerJDialog getUndoTransformationDifferentViewerJDialog()
    {
        return undoTransformationDifferentViewerJDialog;
    }

    /**
     * Getter for the dialog box that is triggered <b>after</b> the user has seen the dialog box from
     * {@link UndoTransformationDifferentViewerJDialog} box and has chosen not to go to the screen where the last
     * transformation occurred, but to undo it anyway. This dialog option warns the user that the screen they're on
     * right now is the result of a transformation, so it will cease to exist if the transformation is undone.
     * @return dialog box for when user tries to undo a transformation without moving away from the current
     * visualisation viewer
     * @see UndoTransformationMergeChildrenJDialog
     */
    public UndoTransformationMergeChildrenJDialog getUndoTransformationMergeChildrenJDialog()
    {
        return undoTransformationMergeChildrenJDialog;
    }

    /**
     * As part of the initiation process, the visualisation viewer object is created once and is reused throughout the
     * program's lifespan. The reason it's reused is that it has listeners attached to it and creating a new
     * visualisation viewer would mean to attach the listeners again.
     * @param graphToDisplay the root, or, the highest hierarchical level, from which the user can navigate to lower
     *                       hierarchical levels
     * @param mergedVertices the collection of merged vertices, which at the beginning will be empty, but it's
     *                       currently a needed parameter for a method inside
     */
    public void createVisualisationViewer
            (
                    NestedGraphTree graphToDisplay,
                    ArrayList<NestedGraphTree> mergedVertices
            )
    {
        Layout<java.lang.Integer, String> layout = new ISOMLayout<>(graphToDisplay.createGraph());
        visualizationViewer = new VisualizationViewer<Integer, String>(layout);

        // In the beginning there must not be an option to go forward/merge, no matter the mode, because no vertex/edge
        // will be selected initially, so we're in the default state
        rightSideScreen.getGoDownButton().setVisible(false);
        rightSideScreen.getMergeButton().setVisible(false);


        // Default mode is picking (so no preview box, as that's a feature of the preview mode)
        rightSideScreen.getPreviewPanel().setVisible(false);
        DefaultModalGraphMouse<String, Number> graphMouse = new DefaultModalGraphMouse<>();
        graphMouse.setMode(ModalGraphMouse.Mode.PICKING);
        visualizationViewer.setGraphMouse(graphMouse);

        // The initial graph will be the root, so the one with index 0
        Graph<Integer, String> graph = graphToDisplay.createGraph();

        // Create visualisation viewer for specified graph
        Layout<Integer, String> layout3 = new ISOMLayout<>(graph);
        visualizationViewer = new VisualizationViewer<Integer, String>(layout3);

        VisualisationViewerTransformers.addTransformers(visualizationViewer, graphToDisplay, mergedVertices);

        visualizationViewer.setGraphMouse(graphMouse);
    }

    /**
     * Updates the GUI by taking the graph that's kept as the current one and drawing it, typically this is called after
     * either a transformation or otherwise an action that would change the current visualisation viewer.
     * @param currentGraph the graph to be drawn
     */
    public void drawGUI
            (
                NestedGraphTree currentGraph
            )
    {
        // Remove anything that's from the old state
        frame.getContentPane().removeAll();
        frame.repaint();

        // Grid bag layout used
        frame.setLayout(new GridBagLayout());

        // Set the count of vertices on the graph and the hierarchical level counter labels
        rightSideScreen.setVertexCountLabel(currentGraph.getChildrenIds().size());
        rightSideScreen.setHierarchyLevelLabel(currentGraph.getHierarchicalLevel());

        // Graph screen that contains visualisation viewer constraints
        GridBagConstraints graphScreenConstraints = new GridBagConstraints();
        graphScreen.setLayout(new BorderLayout());
        graphScreen.setBorder(BorderFactory.createLineBorder(Color.gray));
        graphScreen.add(visualizationViewer);

        graphScreenConstraints.fill = GridBagConstraints.BOTH;
        graphScreenConstraints.weightx = 0.5;
        graphScreenConstraints.weighty = 0.5;
        graphScreenConstraints.gridx = 0;
        graphScreenConstraints.gridy = 0;
        frame.add(graphScreen, graphScreenConstraints);

        // Right side screen that contains graph information constraints
        GridBagConstraints rightSideScreenConstraints = new GridBagConstraints();
        rightSideScreenConstraints.fill = GridBagConstraints.BOTH;
        rightSideScreenConstraints.weightx = 0.1;
        rightSideScreenConstraints.weighty = 0;
        rightSideScreenConstraints.gridx = 1;
        rightSideScreenConstraints.gridy = 0;
        frame.add(rightSideScreen, rightSideScreenConstraints);

        // Add menu bar to frame
        frame.setJMenuBar(graphMenu.getMenuBar());

        // Set size and set visible
        frame.setSize(1400, 800);
        frame.setVisible(true);
    }

    /**
     * Draws the preview visualisation viewer to be placed in the preview panel for when the application is in preview
     * mode and the user wishes to preview a nested graph inside a vertex on the current visualisation viewer.
     * @param previewVisualisationViewer the visualisation viewer to be shrunk and zoomed out, so it can be examined
     *                                   in preview mode easier
     */
    public void drawPreview
            (
                    VisualizationViewer<Integer,String> previewVisualisationViewer
            )
    {
        // Update the preview panel and draw the preview visualisation viewer, which would contain the graph that's
        // nested in the selected vertex in the current graph on the visualisation viewer
        rightSideScreen.getPreviewPanel().removeAll();
        rightSideScreen.getPreviewPanel().add(previewVisualisationViewer, BorderLayout.CENTER);
        rightSideScreen.getPreviewPanel().setVisible(true);
    }

    /**
     * Unselects all currently selected vertices on the current visualisation viewer, usually done either after:
     * <ol type="1">
     *   <li>a transformation or otherwise an action that would change the current visualisation viewer so if the user
     *   goes back to the current visualisation viewer no vertices are selected, or</li>
     *   <li>the user has selected an edge, and in the current implementation a user cannot both select a vertex
     *   and an edge, as it has implications for merging</li>
     * </ol>
     */
    public void unselectAllVertices()
    {
        // Unselect all selected vertices so if we go back to old state they're not selected
        Object[] selected_vertices = visualizationViewer.getPickedVertexState().getSelectedObjects();
        for (Object selected_vertex : selected_vertices) {
            Integer curr_selected_vertex = (Integer) selected_vertex;
            visualizationViewer.getPickedVertexState().pick(curr_selected_vertex, false);
        }
    }

    /**
     * Unselects all currently selected edges on the current visualisation viewer, usually done either after:
     * <ol type="1">
     *   <li>a transformation or otherwise an action that would change the current visualisation viewer so if the user
     *   goes back to the current visualisation viewer no edges are selected, or</li>
     *   <li>the user has selected a vertex, and in the current implementation a user cannot both select a vertex
     *   and an edge, as it has implications for merging</li>
     * </ol>
     */
    public void unselectAllEdges()
    {
        // Unselect all selected edges so if we go back to old state they're not selected
        Object[] selected_edges = visualizationViewer.getPickedEdgeState().getSelectedObjects();
        for (Object selected_edge : selected_edges) {
            String curr_selected_edge = (String) selected_edge;
            visualizationViewer.getPickedEdgeState().pick(curr_selected_edge, false);
        }
    }
}
