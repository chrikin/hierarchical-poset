package org.ottr.graph.views;

import org.ottr.graph.controllers.listeners.NestedGraphButtonListener;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;

/**
 * Holds all elements on the right screen.
 */
public class RightSideScreen extends JPanel
{
    // Graph info labels
    private final JLabel vertexOnScreenLabel = new JLabel("Vertex count: ");
    private final JLabel hierarchyLevelLabel = new JLabel("Hierarchy level: ");

    // Vertex info labels
    private final JLabel vertexIdLabel = new JLabel("No vertex selected yet.");
    private final JLabel vertexAdditionalInfoLabel = new JLabel("Select one vertex for specific information.");
    private final JPanel infoHolderPanel = new JPanel();
    private final JScrollPane infoScroll = new JScrollPane
            (
                    infoHolderPanel,
                    JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                    JScrollPane.HORIZONTAL_SCROLLBAR_NEVER
            );

    // Preview Box
    private final JPanel previewPanel = new JPanel();

    // Action buttons
    private final JButton goUpButton;
    private final JButton goDownButton;
    private final JButton mergeButton;

    /**
     * Class constructor.
     */
    public RightSideScreen()
    {
        Border grayline = BorderFactory.createLineBorder(Color.gray);

        // Right screen
        this.setLayout(new BorderLayout());
        this.setBorder(grayline);
        this.setBackground(Color.decode("#D3D3D3"));

        //Big text box
        JPanel text_box = new JPanel();

        // Title
        JLabel title = new JLabel("INFORMATION SCREEN");
        title.setFont(new Font("SansSerif Bold", Font.BOLD, 25));
        title.setBorder(new EmptyBorder(20, 20, 20, 100));

        // Graph info box
        JPanel graph_info_box = new JPanel();
        graph_info_box.setFont(new Font("SansSerif", Font.PLAIN, 18));
        graph_info_box.setBackground(Color.decode("#D3D3D3"));
        GridLayout graph_info_grid_layout = new GridLayout(0,1);
        graph_info_box.setLayout(graph_info_grid_layout);
        graph_info_box.setBorder
                (
                        BorderFactory.createCompoundBorder
                                (
                                        new EmptyBorder(10, 20, 10, 20),
                                        BorderFactory.createTitledBorder
                                                (
                                                        BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                                                        "Graph Information"
                                                )

                                )

                );

        vertexOnScreenLabel.setFont(new Font("SansSerif", Font.PLAIN, 15));
        vertexOnScreenLabel.setBorder(new EmptyBorder(10, 20, 10, 10));

        hierarchyLevelLabel.setFont(new Font("SansSerif", Font.PLAIN, 15));
        hierarchyLevelLabel.setBorder(new EmptyBorder(10, 20, 10, 10));

        graph_info_box.add(hierarchyLevelLabel);
        graph_info_box.add(vertexOnScreenLabel);

        // Vertex info box
        JPanel vertex_info_box = new JPanel();
        vertex_info_box.setFont(new Font("SansSerif", Font.PLAIN, 18));
        vertex_info_box.setBackground(Color.decode("#D3D3D3"));
        vertex_info_box.setLayout(new BoxLayout(vertex_info_box, BoxLayout.PAGE_AXIS));
        vertex_info_box.setBorder
                (
                        BorderFactory.createCompoundBorder
                                (
                                    new EmptyBorder(0, 20, 30, 20),
                                    BorderFactory.createTitledBorder
                                            (
                                                    BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                                                    "Vertex Information"
                                            )

                                )

                );

        vertexIdLabel.setFont(new Font("SansSerif", Font.PLAIN, 15));
        vertexIdLabel.setBorder(new EmptyBorder(10, 20, 10, 10));
        vertexIdLabel.setAlignmentX(Component.LEFT_ALIGNMENT);

        vertexAdditionalInfoLabel.setFont(new Font("SansSerif", Font.PLAIN, 15));
        vertexAdditionalInfoLabel.setBackground(Color.decode("#D3D3D3"));

        // We have a JPanel info_holder holding the JLabel vertex_additional_info, this way the label is positioned
        // at the top of the panel (using box layout), and then the panel is put in the JScrollPane, so if the
        // label becomes too large the scroll pane can be used to scroll up/down
        infoHolderPanel.setLayout(new BoxLayout(infoHolderPanel, BoxLayout.PAGE_AXIS));
        infoHolderPanel.add(vertexAdditionalInfoLabel);
        infoHolderPanel.setBackground(Color.decode("#D3D3D3"));

        infoScroll.setBorder
                (
                        BorderFactory.createCompoundBorder
                                (
                                        new EmptyBorder(0, 10, 10, 20),
                                        BorderFactory.createCompoundBorder
                                                (
                                                        grayline,
                                                        new EmptyBorder(10, 10, 0, 0)
                                                )

                                )

                );

        infoScroll.setBackground(Color.decode("#D3D3D3"));
        infoScroll.getViewport().setBackground(Color.decode("#D3D3D3"));
        infoScroll.setPreferredSize(new Dimension(0, 130));
        infoScroll.setAlignmentX(Component.LEFT_ALIGNMENT);

        vertex_info_box.add(vertexIdLabel);
        vertex_info_box.add(infoScroll);
        infoScroll.setVisible(false);


        GridBagLayout text_box_layout = new GridBagLayout();
        text_box.setLayout(text_box_layout);

        GridBagConstraints text_box_c = new GridBagConstraints();

        text_box_c.fill = GridBagConstraints.BOTH;
        text_box_c.weightx = 0.3;
        text_box_c.weighty = 0;
        text_box_c.gridx = 0;
        text_box_c.gridy = 0;
        text_box.add(title, text_box_c);

        text_box_c.fill = GridBagConstraints.BOTH;
        text_box_c.weightx = 0.3;
        text_box_c.weighty = 0;
        text_box_c.gridx = 0;
        text_box_c.gridy = 1;
        text_box.add(graph_info_box, text_box_c);

        text_box_c.fill = GridBagConstraints.BOTH;
        text_box_c.weightx = 0.3;
        text_box_c.weighty = 0;
        text_box_c.gridx = 0;
        text_box_c.gridy = 2;
        text_box.add(vertex_info_box, text_box_c);

        text_box.setBackground(Color.decode("#D3D3D3"));

        this.add(text_box, BorderLayout.PAGE_START);


        // Preview Box
        TitledBorder preview_title = BorderFactory.createTitledBorder
                (
                        BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                        "Preview"
                );
        preview_title.setTitlePosition(TitledBorder.ABOVE_TOP);

        previewPanel.setBorder
        (
                BorderFactory.createCompoundBorder
                        (
                                new EmptyBorder(10, 20, 0, 20),
                                preview_title
                        )

        );

        previewPanel.setLayout(new BorderLayout());
        previewPanel.setBackground(Color.decode("#D3D3D3"));

        // Placeholder text
        JPanel preview_box_text_box = new JPanel();
        preview_box_text_box.setLayout(new GridBagLayout());
        preview_box_text_box.setBackground(Color.decode("#D3D3D3"));
        preview_box_text_box.add(new JLabel("Select nested vertex to preview."));

        previewPanel.add(preview_box_text_box, BorderLayout.CENTER);

        this.add(previewPanel, BorderLayout.CENTER);

        // Button box
        JPanel button_box = new JPanel();
        button_box.setBackground(Color.decode("#D3D3D3"));


        // Back box
        JPanel back_button_box = new JPanel();
        back_button_box.setBackground(Color.decode("#D3D3D3"));
        back_button_box.setBorder(new EmptyBorder(20, 0, 20, -10));
        back_button_box.setVisible(true);

        // GO UP
        // Go backwards from nested graphs
        goUpButton = new JButton("GO UP");
        goUpButton.setBackground(Color.LIGHT_GRAY);
        goUpButton.setFont(new Font("SansSerif", Font.PLAIN, 18));
        goUpButton.setPreferredSize(new Dimension(200, 80));
        back_button_box.add(goUpButton);


        // Forward and merge box
        JPanel forward_and_merge_button_box = new JPanel();
        forward_and_merge_button_box.setBackground(Color.decode("#D3D3D3"));
        forward_and_merge_button_box.setBorder(new EmptyBorder(20, -10, 20, 0));
        forward_and_merge_button_box.setVisible(true);

        // GO DOWN
        // Go forwards in nested graphs
        goDownButton = new JButton("GO DOWN");
        goDownButton.setBackground(Color.LIGHT_GRAY);
        goDownButton.setFont(new Font("SansSerif", Font.PLAIN, 18));
        goDownButton.setPreferredSize(new Dimension(200, 80));
        forward_and_merge_button_box.add(goDownButton);

        // MERGE
        // Go backwards from nested graphs
        mergeButton = new JButton("MERGE");
        mergeButton.setBackground(Color.LIGHT_GRAY);
        mergeButton.setFont(new Font("SansSerif", Font.PLAIN, 18));
        mergeButton.setPreferredSize(new Dimension(200, 80));
        forward_and_merge_button_box.add(mergeButton);


        GridLayout button_grid_layout = new GridLayout(0,2);
        button_box.setLayout(button_grid_layout);

        button_box.add(back_button_box);
        button_box.add(forward_and_merge_button_box);

        this.add(button_box, BorderLayout.PAGE_END);

        // Right screen panel constraints
        GridBagConstraints rightSideScreenConstraints = new GridBagConstraints();
        rightSideScreenConstraints.fill = GridBagConstraints.BOTH;
        rightSideScreenConstraints.weightx = 0.1;
        rightSideScreenConstraints.weighty = 0;
        rightSideScreenConstraints.gridx = 1;
        rightSideScreenConstraints.gridy = 0;
    }

    /**
     * Setter for the count displayed on the vertex count label, usually called after the visualisation viewer has been
     * changed and the count of vertices onscreen is likely different. Cannot be negative.
     * @param newCount the new count of vertices to be displayed
     */
    public void setVertexCountLabel(int newCount)
    {
        if(newCount >= 0)
        {
            String new_text = "Vertex count: " + String.valueOf(newCount);
            vertexOnScreenLabel.setText(new_text);
        }
    }

    /**
     * Setter for the count displayed on the hierarchical level label, usually called after the user has navigated one
     * level up/down. Cannot be negative.
     * @param level the new level counter to be displayed
     */
    public void setHierarchyLevelLabel(int level)
    {
        if(level >= 0)
        {
            String new_text = "Hierarchical level: " + String.valueOf(level);
            hierarchyLevelLabel.setText(new_text);
        }
    }

    /**
     * Setter for the id displayed on the currently selected vertex id label, usually called after a new vertex/vertices
     * have been selected.
     * @param newText containing the new id to be displayed or text informing no vertex has been selected
     */
    public void setVertexIdLabel(String newText)
    {
        vertexIdLabel.setText(newText);
    }

    /**
     * Setter for the text displayed on the additional information label, usually called after a new vertex/vertices
     * has been selected.
     * @param newText new additional information text to be displayed
     */
    public void setVertexAdditionalInfoLabel(String newText)
    {
        vertexAdditionalInfoLabel.setText("<html>" + newText + "</html>");
    }

    /**
     * Getter for the info scroll where the additional information text is displayed. A scroll pane is used because
     * often times the additional info string is long, so we need a way to expand it without disrupting the right
     * side screen.
     */
    public JScrollPane getInfoScroll()
    {
        return infoScroll;
    }

    /**
     * Getter for the button whose action event is to navigate to the visualisation viewer that displays the nested
     * graph from the selected vertex. The resulting visualisation viewer is one hierarchical level one below the
     * current one.
     * @see NestedGraphButtonListener#goDownOneLevel()
     */
    public JButton getGoDownButton(){return goDownButton;}

    /**
     * Getter for the button whose action event is to navigate to the visualisation viewer that displays the nested
     * graph from the parent vertex. The resulting visualisation viewer is one hierarchical level above the current
     * one.
     * @see NestedGraphButtonListener#goUpOneLevel()
     */
    public JButton getGoUpButton(){return goUpButton;}

    /**
     * Getter for the button whose action event is to merge more than one vertex. The resulting visualisation viewer
     * contains all the information that it did before, but now there is a new vertex that has replaced the merged ones.
     * @see NestedGraphButtonListener#merge()
     */
    public JButton getMergeButton(){return mergeButton;}

    /**
     * Getter for the panel that holds the preview visualisation viewer. This panel is only visible if the user has
     * selected the preview mode in the {@link org.ottr.menubar.models.MenuMode}.
     */
    public JPanel getPreviewPanel(){return previewPanel;}
}
