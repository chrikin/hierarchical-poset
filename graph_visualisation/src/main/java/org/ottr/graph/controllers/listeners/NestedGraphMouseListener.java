package org.ottr.graph.controllers.listeners;

import edu.uci.ics.jung.algorithms.layout.ISOMLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.visualization.Layer;
import edu.uci.ics.jung.visualization.VisualizationViewer;
import edu.uci.ics.jung.visualization.control.DefaultModalGraphMouse;
import edu.uci.ics.jung.visualization.control.GraphMouseListener;
import edu.uci.ics.jung.visualization.control.ModalGraphMouse;
import org.ottr.graph.models.GraphModel;
import org.ottr.graph.models.representation.NestedGraphTree;
import org.ottr.graph.views.MainView;
import org.ottr.graph.views.VisualisationViewerTransformers;
import org.ottr.menubar.models.MenuMode;

import java.awt.event.MouseEvent;

/**
 * Manages all events from mouse clicks on the graph.
 */
public class NestedGraphMouseListener<Integer> implements GraphMouseListener<Integer>
{
    private final MainView mainView;
    private final GraphModel graphModel;

    /**
     * Class constructor.
     * @param graphModel the model of the application
     * @param mainView the main view of the application
     */
    public NestedGraphMouseListener
            (
                    GraphModel graphModel,
                    MainView mainView
            )
    {
        this.graphModel = graphModel;
        this.mainView = mainView;
    }

    /**
     * If the user double left-clicks on a vertex, if the vertex is deep (refer to
     * {@link NestedGraphTree#isVertexDeep()}, then the current graph screen is changed and the visualisation viewer is
     * updated with the new nested graph within the child.
     * If the user right clicks while in preview mode in the {@link MenuMode} on a vertex and the vertex is deep
     * {@link NestedGraphTree#isVertexDeep()}, then a smaller version of the graph nested inside it will appear in the
     * preview box.
     * @param selected_vertex_id the id of vertex selected
     * @param mouse_event the event to be processed
     */
    @Override
    public void graphClicked(Integer selected_vertex_id, MouseEvent mouse_event)
    {
        // Double-clicking a vertex with left button leads to deeper level, iff
        // vertex has a deeper level, and
        // not in preview mode
        if (mouse_event.getButton() == MouseEvent.BUTTON1 && mouse_event.getClickCount() == 2 && graphModel.getMode().getChosenMode() == MenuMode.mode.PICKING)
        {
            // We need to check the selected vertex has a nested graph inside it to explore
            boolean is_selected_vertex_deep = graphModel.getCurrentGraph().getChildWithId((java.lang.Integer) selected_vertex_id).isVertexDeep();

            if (is_selected_vertex_deep)
            {
                mainView.unselectAllVertices();
                mainView.unselectAllEdges();

//                       // Add in histories
//                       graph_data.graph_level_history.push(vv);
//                       graph_data.vertex_count_history.push(curr_graph_vertices.size());

                graphModel.setCurrentGraph(graphModel.getCurrentGraph().getChildWithId((java.lang.Integer) selected_vertex_id));

                // New visualisation viewer to display
                Layout<java.lang.Integer, String> layout = new ISOMLayout<>(graphModel.getCurrentGraph().createGraph());
                mainView.getVisualizationViewer().setGraphLayout(layout);
                VisualisationViewerTransformers.addTransformers(mainView.getVisualizationViewer(), graphModel.getCurrentGraph(), graphModel.getMergedVertices());

                // If gone into deeper level (double click) right screen should be empty
                // and no option to go deeper should appear
                mainView.getRightSideScreen().setVertexIdLabel("No vertex selected yet.");
                mainView.getRightSideScreen().setVertexAdditionalInfoLabel("Select one vertex for specific information.");
                mainView.getRightSideScreen().getGoDownButton().setVisible(false);

                // If gone deeper there must be at least one layer to go back to,
                // so option must be available
                mainView.getRightSideScreen().getGoUpButton().setVisible(true);

                mainView.drawGUI(graphModel.getCurrentGraph());
            }
        }

        // Right-clicking a vertex shows a preview of the nested graph inside it, iff
        // vertex has a deeper level, and
        // in preview mode
        else if (mouse_event.getButton() == MouseEvent.BUTTON3 && graphModel.getMode().getChosenMode() == MenuMode.mode.PREVIEWING)
        {
            boolean is_selected_vertex_deep = graphModel.getCurrentGraph().getChildWithId((java.lang.Integer) selected_vertex_id).isVertexDeep();

            if (is_selected_vertex_deep)
            {
                mainView.unselectAllVertices();
                mainView.unselectAllEdges();

                // Select vertex to be previewed
                mainView.getVisualizationViewer().getPickedVertexState().pick((java.lang.Integer) selected_vertex_id, true);

                graphModel.setCurrentPreviewGraph(graphModel.getCurrentGraph().getChildWithId((java.lang.Integer) selected_vertex_id));
                // Get preview visualisation viewer for preview of selected vertex
                Layout<java.lang.Integer, String> layout = new ISOMLayout<>(graphModel.getCurrentPreviewGraph().createGraph());
                VisualizationViewer<java.lang.Integer, String> preview_vv = new VisualizationViewer<>(layout);

                // Default mode in preview box is transforming, so you can move around if graph is too big
                DefaultModalGraphMouse<String, Number> preview_graph_mouse = new DefaultModalGraphMouse<>();
                preview_vv.setGraphMouse(preview_graph_mouse);
                preview_graph_mouse.setMode(ModalGraphMouse.Mode.TRANSFORMING);
                VisualisationViewerTransformers.addTransformers((VisualizationViewer<java.lang.Integer, String>) preview_vv, graphModel.getCurrentGraph().getChildWithId((java.lang.Integer) selected_vertex_id), graphModel.getMergedVertices());

                // Zoom out
                preview_vv.getRenderContext().getMultiLayerTransformer().getTransformer(Layer.VIEW).setScale(0.5, 0.5, preview_vv.getCenter());

                mainView.drawPreview((VisualizationViewer<java.lang.Integer, String>) preview_vv);

            }
            mouse_event.consume();
        }
    }

    @Override
    public void graphPressed(Integer selected_vertex_id, MouseEvent mouse_event)
    {}

    @Override
    public void graphReleased(Integer selected_vertex_id, MouseEvent mouse_event)
    {}
}
