package org.ottr.graph.controllers.listeners;

import edu.uci.ics.jung.algorithms.layout.ISOMLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import org.ottr.graph.models.representation.Edge;
import org.ottr.graph.views.RightSideScreen;
import org.ottr.menubar.models.MenuMode;
import org.ottr.transformations.models.Merge;
import org.ottr.graph.models.GraphModel;
import org.ottr.graph.models.representation.NestedGraphTree;
import org.ottr.graph.views.MainView;
import org.ottr.graph.views.VisualisationViewerTransformers;
import org.ottr.graph.models.representation.InformationObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/**
 * Manages all events from buttons within the application.
 */
public class NestedGraphButtonListener
{
    private final MainView mainView;
    private final GraphModel graphModel;

    /**
     * Class constructor.
     * @param graphModel the model of the application
     * @param mainView the main view of the application
     */
    public NestedGraphButtonListener
            (
                    GraphModel graphModel,
                    MainView mainView
            )
    {
        this.graphModel = graphModel;
        this.mainView = mainView;
    }

    /**
     * Navigates the internal graph structure one hierarchical level down, given a NestedGraphTree object to navigate to,
     * so for example if the current graph is represented by the NestedGraphTree object with id 1 and it's children are
     * represented by 2 and 3, if either of the children have a graph nested inside them, a user can navigate one level
     * down to one of them. Listener for the GO DOWN button from the view {@link RightSideScreen#getGoDownButton()}.
     */
    public void goDownOneLevel()
    {
        Object[] selectedVertices = mainView.getVisualizationViewer().getPickedVertexState().getSelectedObjects();
        if(selectedVertices.length == 1 && graphModel.getMode().getChosenMode() == MenuMode.mode.PICKING)
        {
            // Unselect all selected vertices so if we go back they're not selected
            mainView.unselectAllVertices();
            // Unselect all selected edges so if we go back they're not selected
            mainView.unselectAllEdges();

            // We want to go into the child vertex
            Integer selectedVertexId = (Integer) selectedVertices[0];
            graphModel.setCurrentGraph(graphModel.getCurrentGraph().getChildWithId(selectedVertexId));
            Layout<Integer, String> layout = new ISOMLayout<>(graphModel.getCurrentGraph().createGraph());
            mainView.getVisualizationViewer().setGraphLayout(layout);

            // Add vertex and edge transformers
            VisualisationViewerTransformers.addTransformers(mainView.getVisualizationViewer(), graphModel.getCurrentGraph(), graphModel.getMergedVertices());

            // If gone into deeper level (double click) right screen should be empty
            // and no option to go deeper should appear
            mainView.getRightSideScreen().setVertexIdLabel("No vertex selected yet.");
            mainView.getRightSideScreen().setVertexAdditionalInfoLabel("Select one vertex for specific information.");
            mainView.getRightSideScreen().getGoDownButton().setVisible(false);

            // If gone deeper there must be at least one layer to go back to,
            // so option must be available
            mainView.getRightSideScreen().getGoUpButton().setVisible(true);

            mainView.drawGUI(graphModel.getCurrentGraph());
        }
    }

    /**
     * Navigates the internal graph structure one hierarchical level up, given the current NestedGraphTree object isn't
     * a root, so for example if the current NestedGraphTree object isn't a root and has id 2, but its parent has id 1,
     * the user could go up a level to the parent with id 1. Listener for the GO UP button from the view {@link
     * RightSideScreen#getGoUpButton()}.
     */
    public void goUpOneLevel()
    {
        // Do only if there is something to go back to
        if(graphModel.getCurrentGraph().getParent() != null)
        {
            // Unselect all selected vertices so if we go back they're not selected
            mainView.unselectAllVertices();
            // Unselect all selected edges so if we go back they're not selected
            mainView.unselectAllEdges();

            // We want to go back to the parent of the vertex
            graphModel.setCurrentGraph(graphModel.getCurrentGraph().getParent());
            Layout<Integer, String> layout = new ISOMLayout<>(graphModel.getCurrentGraph().createGraph());
            mainView.getVisualizationViewer().setGraphLayout(layout);

            // Add vertex and edge transformers
            VisualisationViewerTransformers.addTransformers(mainView.getVisualizationViewer(), graphModel.getCurrentGraph(), graphModel.getMergedVertices());

            // If no parent above, make back button disappear
            if(graphModel.getCurrentGraph().getParent() == null)
            {
                mainView.getRightSideScreen().getGoUpButton().setVisible(false);
            }
            mainView.drawGUI(graphModel.getCurrentGraph());
        }
    }

    /**
     * Merges more than one selected NestedGraphTree objects into one. The resulting object has all the children of the
     * children, children vertex id's, edges, and information objects of the merged NestedGraphTree objects, with having
     * the same parent as them as well. It calls a specific NestedGraphTree constructor designed for the merge
     * operation. Listener for the MERGE button from the view {@link RightSideScreen#getMergeButton()}.
     */
    public void merge()
    {
        // If selected edges are one, must contract an edge
        Object[] selected_edges = mainView.getVisualizationViewer().getPickedEdgeState().getSelectedObjects();
        if(selected_edges.length == 1)
        {
            int start_vertex_id = -1;
            int end_vertex_id = -1;
            String selected_edge = ((String) selected_edges[0]);

            // Splitting symbol currently is "-", so an edge between vertices with id's 1 and 2 would
            // be defined by "1-2"
            String[] start_and_end = selected_edge.split("-");

            boolean valid_ints = true;

            // Make sure all conversions are legal (converting string -> int)
            try
            {
                start_vertex_id = Integer.parseInt(start_and_end[0]);
            }
            catch(NumberFormatException ex)
            {
                System.out.println("Attempted to convert invalid integer string");
                valid_ints = false;
            }
            try
            {
                end_vertex_id = Integer.parseInt(start_and_end[1]);
            }
            catch(NumberFormatException ex)
            {
                System.out.println("Attempted to convert invalid integer string");
                valid_ints = false;
            }

            if(valid_ints)
            {
                // Make sure no vertices selected
                mainView.unselectAllVertices();

                // Select start and end vertices of edge
                mainView.getVisualizationViewer().getPickedVertexState().pick(start_vertex_id, true);
                mainView.getVisualizationViewer().getPickedVertexState().pick(end_vertex_id, true);

            }
        }
        // Vertices to merge (must be more than 1 if button has been revealed)
        Object[] vertexIdsToBeMergedArr = mainView.getVisualizationViewer().getPickedVertexState().getSelectedObjects();

        // Unselect all
        mainView.unselectAllEdges();
        mainView.unselectAllVertices();

        // Ids of vertices to be merged
        ArrayList<Integer> vertexIdsToBeMerged = new ArrayList(Arrays.asList(vertexIdsToBeMergedArr));
        // Vertices to be merged
        ArrayList<NestedGraphTree> verticesToBeMerged = new ArrayList<>();

        HashSet<NestedGraphTree> allVerticesToBeMergedChildren = new HashSet<>();
        HashSet<Edge> allVerticesToBeMergedEdges = new HashSet<>();
        ArrayList<InformationObject> allVerticesToBeMergedInfoObj = new ArrayList<>();

        // MERGING

        // Create merged vertex with from all vertices to be merged
        for (Integer currVertexToBeMergedId : vertexIdsToBeMerged)
        {
            allVerticesToBeMergedChildren.addAll
                    (
                            graphModel.getCurrentGraph().getChildWithId(currVertexToBeMergedId).getChildren()
                    );
            allVerticesToBeMergedEdges.addAll
                    (
                            graphModel.getCurrentGraph().getChildWithId(currVertexToBeMergedId).getEdges()
                    );
            allVerticesToBeMergedInfoObj.add
                    (
                            graphModel.getCurrentGraph().getChildWithId(currVertexToBeMergedId).getInformationObject()
                    );
        }

        InformationObject mergedInformationObject = new InformationObject(allVerticesToBeMergedInfoObj, vertexIdsToBeMerged);

        NestedGraphTree mergedVertex = new NestedGraphTree
                (
                        graphModel.getVertexCounter(),
                        false,
                        true,
                        graphModel.getCurrentGraph().getChildWithId(vertexIdsToBeMerged.get(0)).getParent(),
                        allVerticesToBeMergedChildren,
                        allVerticesToBeMergedEdges,
                        mergedInformationObject
                );

        Merge merge = new Merge
                (
                        graphModel.getCurrentGraph().getId(),
                        graphModel.getCurrentGraph().hashCode(),
                        graphModel.getCurrentGraph().getChildrenIds(),
                        graphModel.getCurrentGraph().getChildren(),
                        graphModel.getCurrentGraph().getEdges(),
                        mergedVertex
                );

        graphModel.getTransformationHistory().addTransformationToHistory(merge);

        graphModel.incrementVertexCounterWith(1);
        graphModel.getMergedVertices().add(mergedVertex);
        graphModel.getAllVertices().put(mergedVertex.hashCode(), mergedVertex);

        // Set merged vertex children's parent - the merged vertex
        for (Integer vertexIdToBeMerged : vertexIdsToBeMerged)
        {
            HashSet<NestedGraphTree> children = graphModel.getCurrentGraph().getChildWithId(vertexIdToBeMerged).getChildren();
            for(NestedGraphTree child: children)
            {
                child.setParent(mergedVertex);
            }
        }


        HashSet<NestedGraphTree> currentVerticesOnScreen = graphModel.getCurrentGraph().getChildren();
        HashSet<NestedGraphTree> verticesOnScreenAfterMerge = new HashSet<>();
        for(NestedGraphTree childOnScreen: currentVerticesOnScreen)
        {
            if(!vertexIdsToBeMerged.contains(childOnScreen.getId()))
            {
                verticesOnScreenAfterMerge.add(childOnScreen);
            }
        }
        verticesOnScreenAfterMerge.add(mergedVertex);

        ArrayList<Integer> vertexIdsOnScreenAfterMerge = new ArrayList<>();
        for(NestedGraphTree vertexVisibleAfterMerge: verticesOnScreenAfterMerge)
        {
            vertexIdsOnScreenAfterMerge.add(vertexVisibleAfterMerge.getId());
        }

        HashSet<Edge> currentEdgesOnScreen = graphModel.getCurrentGraph().getEdges();
        HashSet<Edge> edgesOnScreenAfterMerge = new HashSet<>();
        for(Edge currEdgeOnScreen: currentEdgesOnScreen)
        {
            if(!vertexIdsToBeMerged.contains(currEdgeOnScreen.getStartVertex().getId()))
            {
                if(!vertexIdsToBeMerged.contains(currEdgeOnScreen.getDestinationVertex().getId()))
                {
                    edgesOnScreenAfterMerge.add(currEdgeOnScreen);
                }
                else
                {
                    currEdgeOnScreen.setDestinationVertex(mergedVertex);
                    edgesOnScreenAfterMerge.add(currEdgeOnScreen);
                }
            }
            else
            {
                if(!vertexIdsToBeMerged.contains(currEdgeOnScreen.getDestinationVertex().getId()))
                {
                    currEdgeOnScreen.setStartVertex(mergedVertex);
                    edgesOnScreenAfterMerge.add(currEdgeOnScreen);
                }
                else
                {
                    // Nothing should happen
                }
            }
        }

        graphModel.getCurrentGraph().setChildren(verticesOnScreenAfterMerge);
        graphModel.getCurrentGraph().setEdges(edgesOnScreenAfterMerge);


        // Create graph
        Layout<Integer, String> layout = new ISOMLayout<>(graphModel.getCurrentGraph().createGraph());
        mainView.getVisualizationViewer().setGraphLayout(layout);

        // Add vertex and edge transformers
        VisualisationViewerTransformers.addTransformers(mainView.getVisualizationViewer(), graphModel.getCurrentGraph(), graphModel.getMergedVertices());

        // If parent above, make back button appear
        if(graphModel.getCurrentGraph().getParent() != null)
        {
            mainView.getRightSideScreen().getGoUpButton().setVisible(true);
        }
        else
        {
            mainView.getRightSideScreen().getGoUpButton().setVisible(false);
        }

        // If merged nothing selected
        mainView.getRightSideScreen().setVertexIdLabel("No vertex selected yet.");
        mainView.getRightSideScreen().setVertexAdditionalInfoLabel("Select one vertex for specific information.");
        mainView.getRightSideScreen().getGoDownButton().setVisible(false);
        mainView.getRightSideScreen().getMergeButton().setVisible(false);

        mainView.drawGUI(graphModel.getCurrentGraph());
    }
}
