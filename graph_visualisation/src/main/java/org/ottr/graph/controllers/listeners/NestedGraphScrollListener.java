package org.ottr.graph.controllers.listeners;

import edu.uci.ics.jung.algorithms.layout.ISOMLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import org.ottr.graph.models.GraphModel;
import org.ottr.graph.views.MainView;
import org.ottr.graph.views.VisualisationViewerTransformers;

import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

/**
 * Manages all events from mouse scrolls on the graph.
 */
public class NestedGraphScrollListener implements MouseWheelListener
{
    private final MainView mainView;
    private final GraphModel graphModel;

    /**
     * Class constructor.
     * @param graphModel the model of the application
     * @param mainView the main view of the application
     */
    public NestedGraphScrollListener
            (
                    GraphModel graphModel,
                    MainView mainView
            )
    {
        this.graphModel = graphModel;
        this.mainView = mainView;
    }

    /**
     * If the user holds down ctrl and scrolls back, then with ever zoom unit the user will go up one
     * hierarchical level. This is identical to using the GO UP button however many times you want to go up.
     * @param e the event to be processed
     */
    @Override
    public void mouseWheelMoved(MouseWheelEvent e)
    {
        // CTRL and scroll goes back a hierarchical level, iff
        // there is a prior hierarchical level, and
        // not in preview mode
        if(e.isControlDown() && !graphModel.getCurrentGraph().isRoot())
        {
            int zoom_unit = e.getWheelRotation();
            // If positive it's scroll back, go back in hierarchical levels
            if(zoom_unit > 0)
            {
                // If parent above, make back button appear
                if(graphModel.getCurrentGraph().getParent() != null)
                {
                    mainView.getRightSideScreen().getGoUpButton().setVisible(true);
                }
                else
                {
                    mainView.getRightSideScreen().getGoUpButton().setVisible(false);
                }

                // We want to go back to the parent of the vertex
                graphModel.setCurrentGraph(graphModel.getCurrentGraph().getParent());
                Layout<Integer, String> layout = new ISOMLayout<>(graphModel.getCurrentGraph().createGraph());
                mainView.getVisualizationViewer().setGraphLayout(layout);

                // Add vertex and edge transformers
                VisualisationViewerTransformers.addTransformers(mainView.getVisualizationViewer(), graphModel.getCurrentGraph(), graphModel.getMergedVertices());

                // If no parent above, make back button disappear
                if(graphModel.getCurrentGraph().getParent() == null)
                {
                    mainView.getRightSideScreen().getGoUpButton().setVisible(false);
                }
                mainView.drawGUI(graphModel.getCurrentGraph());
            }
        }
    }
}
