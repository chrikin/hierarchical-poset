package org.ottr.graph.controllers.listeners;

import com.google.common.base.Function;
import org.ottr.graph.models.GraphModel;
import org.ottr.graph.views.MainView;
import org.ottr.menubar.models.MenuMode;

import java.awt.*;
import java.util.Arrays;
import java.util.Objects;

/**
 * Manages all events from vertex selection.
 */
public class NestedGraphVertexSelectionListener
{
    private final MainView mainView;
    private final GraphModel graphModel;

    /**
     * Class constructor.
     * @param graphModel the model of the application
     * @param mainView the main view of the application
     */
    public NestedGraphVertexSelectionListener
            (
                    GraphModel graphModel,
                    MainView mainView
            )
    {
        this.graphModel = graphModel;
        this.mainView = mainView;
    }

    /**
     * Gets called each time there is a change within the selected vertices (includes a new vertex being selected or a
     * vertex being unselected). Currently, selection events are as follows:
     * <ul>
     *   <li>If no vertices are selected - nothing happens.</li>
     *   <li>If only one vertex is selected - displays information about this vertex on the right side screen.</li>
     *   <li>more than vertices are selected - option to merge selected vertices appears.</li>
     * </ul>
     * It's also important to mention vertex selection has an effect on the vertex and font label colour of the selected
     * vertex.
     */
    public void selectVertex()
    {
        {
            // Array of currently selected vertices
            Object[] selected_vertices = mainView.getVisualizationViewer().getPickedVertexState().getSelectedObjects();

            // When an item listener for vertices/edges is called, the program clears the selection list for the other
            // one - so if you call the vertex item listener, the edge list will be cleared (this is done to avoid
            // having both edges and vertices selected at the same time

            // We use the protect flags to save ourselves from falling into a loop that will clear all selections; if we
            //  have 5 edges selected, and then we multi-select 3 vertices, they'll be added to the list one by one, so
            // first we add vertex A, which will trigger the loop to clear all edges, so edge 1 will be cleared, but
            // that will trigger all vertices to be cleared (as that's a change in the edge item listener). Then edge 2,
            // and 3 will be removed. After, vertex B will be added and then C - but A will still be missing!
            if (!graphModel.isProtectEdges())
            {
                graphModel.setProtectVertices(true);
                // Unselect all selected edges so only either vertices or edges are selected
                Object[] selected_edges = mainView.getVisualizationViewer().getPickedEdgeState().getSelectedObjects();
                for (Object selected_edge : selected_edges)
                {
                    String curr_selected_edge = (String) selected_edge;
                    mainView.getVisualizationViewer().getPickedEdgeState().pick(curr_selected_edge, false);
                }
            }
            graphModel.setProtectVertices(false);

            // About vertex selection colouring:
            // If a vertex is merged,
            //                  and it's selected    - it's light blue
            //                  and it's unselected  - it's dark blue

            // If a vertex in non-merged,
            //                  and it's selected,
            //                      and it holds a nested graph within itself - it's dark yellow
            //                      and it doesn't hold a graph within itself - it's light yellow
            //                  and it's unselected,
            //                      and it holds a nested graph within itself - it's dark red
            //                      and it doesn't hold a graph within itself - it's light red
            mainView.getVisualizationViewer().getRenderContext().setVertexFillPaintTransformer(new Function<Integer, Paint>()
            {
                @Override
                public Paint apply(Integer vertex_id) {
                    boolean is_selected_vertex_deep = graphModel.getCurrentGraph().getChildWithId(vertex_id).isVertexDeep();
                    boolean is_selected_vertex_merged = graphModel.getCurrentGraph().getChildWithId(vertex_id).isMerged();

                    // If SELECTED, colour with yellow/blue for non-merged/merged
                    if (Arrays.asList(selected_vertices).contains(vertex_id))
                    {
                        // If SELECTED and MERGED colour with light blue (doesn't matter if deep or shallow)
                        if (is_selected_vertex_merged) {
                            return Color.decode("#00ffff");
                        }
                        // If SELECTED and NON-MERGED
                        else {
                            // If vertex is SELECTED, NON-MERGED and is DEEP, colour with darker yellow
                            if (is_selected_vertex_deep) {
                                return Color.decode("#ffa700");
                            }
                            // If vertex is SELECTED, NON-MERGED and is NOT DEEP, colour with lighter yellow
                            else {
                                return Color.decode("#ffff00");
                            }
                        }
                    } else {
                        // If NOT SELECTED and MERGED colour with dark blue (doesn't matter if deep or shallow)
                        if (is_selected_vertex_merged) {
                            return Color.decode("#0000ff");
                        }
                        // If NOT SELECTED and NON-MERGED
                        else {
                            // If vertex is NOT SELECTED, NON-MERGED and is DEEP, colour with darker red
                            if (is_selected_vertex_deep) {
                                return Color.decode("#ff0000");
                            }
                            // If vertex is NOT SELECTED, NON-MERGED and is NOT DEEP, colour with lighter red
                            else {
                                return Color.decode("#ff6865");
                            }
                        }
                    }
                }
            });

            // Set new selected vertices' information (if 0, 1, or more)
            if (selected_vertices.length == 0) {
                // If clicked on blank space right screen should be empty
                // and no option to expand/merge should appear
                mainView.getRightSideScreen().setVertexIdLabel("No vertex selected yet.");
                mainView.getRightSideScreen().setVertexAdditionalInfoLabel("Select one vertex for specific information.");
                mainView.getRightSideScreen().getInfoScroll().setVisible(false);
                mainView.getRightSideScreen().getGoDownButton().setVisible(false);
                mainView.getRightSideScreen().getMergeButton().setVisible(false);
            }
            else if (selected_vertices.length == 1)
            {

                // Show go into button option if option is available
                // We need to check the selected vertex has a nested graph inside it to explore
                int vertex_id = (Integer) selected_vertices[0];

                boolean is_vertex_deep = graphModel.getCurrentGraph().getChildWithId(vertex_id).isVertexDeep();

                if (is_vertex_deep && graphModel.getMode().getChosenMode() != MenuMode.mode.PREVIEWING)
                {
                    mainView.getRightSideScreen().getGoDownButton().setVisible(true);
                } else {
                    mainView.getRightSideScreen().getGoDownButton().setVisible(false);
                }

                mainView.getRightSideScreen().setVertexIdLabel("Selected vertex: " + vertex_id);

                String additional_information = graphModel.getCurrentGraph().getChildWithId(vertex_id).getInformationObject().getInfoObjectAsString();

                if (Objects.equals(additional_information, "")) {
                    mainView.getRightSideScreen().setVertexAdditionalInfoLabel("No additional information provided.");
                } else {
                    mainView.getRightSideScreen().setVertexAdditionalInfoLabel("Additional information: " + additional_information);
                }

                // No option to merge should appear
                mainView.getRightSideScreen().getMergeButton().setVisible(false);
                mainView.getRightSideScreen().getInfoScroll().setVisible(true);
                mainView.getRightSideScreen().getInfoScroll().setPreferredSize(new Dimension(0, 130));
            }
            // More than one vertex selected
            else
            {
                Arrays.sort(selected_vertices);
                mainView.getRightSideScreen().setVertexIdLabel("Selected vertices: " + Arrays.toString(selected_vertices).substring(1, Arrays.toString(selected_vertices).length() - 1));
                mainView.getRightSideScreen().setVertexAdditionalInfoLabel("Select one vertex for specific information.");
                mainView.getRightSideScreen().getGoDownButton().setVisible(false);
                if (graphModel.getMode().getChosenMode() != MenuMode.mode.PREVIEWING) {
                    mainView.getRightSideScreen().getMergeButton().setVisible(true);
                }
                mainView.getRightSideScreen().getInfoScroll().setVisible(true);
                mainView.getRightSideScreen().getInfoScroll().setPreferredSize(new Dimension(0, 50));
            }
        }
    }
}
