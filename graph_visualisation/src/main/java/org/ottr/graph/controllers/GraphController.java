package org.ottr.graph.controllers;

import org.ottr.transformations.controllers.UndoTransformationDifferentViewerJDialogListener;
import org.ottr.transformations.controllers.UndoTransformationMergeChildrenJDialogListener;
import org.ottr.menubar.controllers.FileMenuActionListener;
import org.ottr.menubar.controllers.ModeMenuActionListener;
import org.ottr.graph.controllers.listeners.*;
import org.ottr.graph.models.GraphModel;
import org.ottr.graph.views.MainView;

import java.io.IOException;

/**
 * Links all events within the application to controllers.
 */
public class GraphController
{
    private final GraphModel graphModel;
    private final MainView mainView;

    /**
     * Class constructor.
     * @param graphModel the model of the application
     * @param mainView the main view of the application
     */
    public GraphController
            (
                    GraphModel graphModel,
                    MainView mainView
            )
    {
        this.graphModel = graphModel;
        this.mainView = mainView;
        initView();
    }

    /**
     * Initiates the main view and all relevant views associated with it.
     */
    public void initView()
    {
        // Create basic visualisation viewer
        mainView.createVisualisationViewer
                (
                        graphModel.getRoot(),
                        graphModel.getMergedVertices()
                );

        // Add vertex and edge transformers to visualisation viewer
        mainView.drawGUI(graphModel.getCurrentGraph());

    }

    /**
     * Adds all event listeners to their relevant GUI elements (buttons, radio buttons, etc) in the views.
     */
    public void initController()
    {
        // Create hash map of all vertices and set vertex counter from hash map size
        graphModel.setAllVertices(graphModel.getRoot().collectAllVerticesInMap());
        graphModel.setVertexCounter(graphModel.getAllVertices().size());

        // MENU BAR EVENTS

        // Menu option file events
        FileMenuActionListener fileMenuActionListener = new FileMenuActionListener(graphModel, mainView);
        mainView.getGraphMenu().getFileMenuOpenButton().addActionListener(e -> {
            try
            {
                fileMenuActionListener.openGraphFile();
            } catch (Exception ex)
            {
                throw new RuntimeException(ex);
            }
        });
        mainView.getGraphMenu().getFileMenuSaveButton().addActionListener(e -> {
            try
            {
                fileMenuActionListener.saveWorkingGraphToSaveDir();
            }
            catch (IOException ex)
            {
                throw new RuntimeException(ex);
            }
        });
        mainView.getGraphMenu().getFileMenuSaveAsButton().addActionListener(e -> {
            try
            {
                fileMenuActionListener.saveWorkingGraphToSpecifiedDir();
            }
            catch (IOException ex)
            {
                throw new RuntimeException(ex);
            }
        });

        // Menu option mode events
        ModeMenuActionListener modeMenuActionListener = new ModeMenuActionListener(graphModel, mainView);
        mainView.getGraphMenu().getModeMenuPickingRadioButton().addActionListener(e -> modeMenuActionListener.selectModePicking());
        mainView.getGraphMenu().getModeMenuTransformingRadioButton().addActionListener(e -> modeMenuActionListener.selectModeTransforming());
        mainView.getGraphMenu().getModeMenuPreviewingRadioButton().addActionListener(e -> modeMenuActionListener.selectModePreviewing());


        // TRANSFORMATION HISTORY EVENTS
        // Undo transformation different viewer JDialog events
        UndoTransformationDifferentViewerJDialogListener undoTransformationDifferentViewerJDialogListener = new UndoTransformationDifferentViewerJDialogListener(graphModel, mainView);
        mainView.getUndoTransformationDifferentViewerJDialog().getGoAndUndoButton().addActionListener(e -> undoTransformationDifferentViewerJDialogListener.changeViewerAndUndoMerge());
        mainView.getUndoTransformationDifferentViewerJDialog().getUndoDontGoButton().addActionListener(e -> undoTransformationDifferentViewerJDialogListener.undoMergeDontChangeViewer());
        mainView.getUndoTransformationDifferentViewerJDialog().getCancelButton().addActionListener(e -> undoTransformationDifferentViewerJDialogListener.cancel());

        // Undo transformation viewer looking at merged vertex graph JDialog events
        UndoTransformationMergeChildrenJDialogListener undoTransformationMergeChildrenJDialogListener = new UndoTransformationMergeChildrenJDialogListener(graphModel, mainView);
        mainView.getUndoTransformationMergeChildrenJDialog().getGoBackOnceAndUndoButton().addActionListener(e -> undoTransformationMergeChildrenJDialogListener.changeViewerOneStepBackAndUndo());
        mainView.getUndoTransformationMergeChildrenJDialog().getCancelButton().addActionListener(e -> undoTransformationMergeChildrenJDialogListener.cancel());


        // GRAPH INTERACTION EVENTS

        // Graph click events
        mainView.getVisualizationViewer().addGraphMouseListener(new NestedGraphMouseListener<Integer>(graphModel, mainView));

        // Graph scroll events
        mainView.getVisualizationViewer().addMouseWheelListener(new NestedGraphScrollListener(graphModel, mainView));

        // Graph key events
        mainView.getVisualizationViewer().addKeyListener(new NestedGraphKeyListener(graphModel, mainView));

        // Vertex selection events
        NestedGraphVertexSelectionListener nestedGraphVertexSelectionListener = new NestedGraphVertexSelectionListener(graphModel, mainView);
        mainView.getVisualizationViewer().getPickedVertexState().addItemListener(e -> nestedGraphVertexSelectionListener.selectVertex());

        // Edge selection events
        NestedGraphEdgeSelectionListener nestedGraphEdgeSelectionListener = new NestedGraphEdgeSelectionListener(graphModel, mainView);
        mainView.getVisualizationViewer().getPickedEdgeState().addItemListener(e -> nestedGraphEdgeSelectionListener.selectEdge());

        // Button events
        NestedGraphButtonListener buttonEventController = new NestedGraphButtonListener(graphModel, mainView);
        mainView.getRightSideScreen().getGoUpButton().addActionListener(e -> buttonEventController.goUpOneLevel());
        mainView.getRightSideScreen().getGoDownButton().addActionListener(e -> buttonEventController.goDownOneLevel());
        mainView.getRightSideScreen().getMergeButton().addActionListener(e -> buttonEventController.merge());
    }
}
