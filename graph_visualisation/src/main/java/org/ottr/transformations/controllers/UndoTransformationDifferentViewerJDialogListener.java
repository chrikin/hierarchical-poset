package org.ottr.transformations.controllers;

import edu.uci.ics.jung.algorithms.layout.ISOMLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import org.ottr.graph.models.GraphModel;
import org.ottr.graph.models.representation.NestedGraphTree;
import org.ottr.graph.views.MainView;
import org.ottr.graph.views.VisualisationViewerTransformers;
import org.ottr.transformations.models.Merge;

/**
 * Manages the events from different viewer than current dialog buttons.
 */
public class UndoTransformationDifferentViewerJDialogListener
{
    private final MainView mainView;
    private final GraphModel graphModel;

    /**
     * Class constructor.
     * @param graphModel the model of the application
     * @param mainView the main view of the application
     */
    public UndoTransformationDifferentViewerJDialogListener
            (
                    GraphModel graphModel,
                    MainView mainView
            )
    {
        this.graphModel = graphModel;
        this.mainView = mainView;
    }

    /**
     * Changes the current graph that is being visualised in the visualisation viewer with the graph on which the last
     * transformation was performed and undoing the transformation simultaneously, effectively restoring it to the state
     * before that transformation.
     */
    public void changeViewerAndUndoMerge()
    {
        Merge lastMerge = (Merge) graphModel.getTransformationHistory().popLastTransformation();

        // Unselect all selected vertices so if we go back they're not selected
        mainView.unselectAllVertices();
        // Unselect all selected edges so if we go back they're not selected
        mainView.unselectAllEdges();

        // Get graph from hash code
        graphModel.setCurrentGraph(graphModel.getAllVertices().get(lastMerge.getHashCodeOfTreeObjTransformationOccurredOn()));

        graphModel.getCurrentGraph().setChildren(lastMerge.getOldGraphChildren());
        graphModel.getCurrentGraph().setEdges(lastMerge.getOldGraphEdges());

        // Fix parent links of children of merged vertex
        for(NestedGraphTree child : graphModel.getCurrentGraph().getChildren())
        {
            for(NestedGraphTree childOfChild : child.getChildren())
            {
                childOfChild.setParent(child);
            }
        }

        Graph<Integer, String> graphToDraw = graphModel.getCurrentGraph().createGraph();
        // Create graph
        Layout<Integer, String> layout = new ISOMLayout<>(graphToDraw);
        mainView.getVisualizationViewer().setGraphLayout(layout);

        // Add vertex and edge transformers
        VisualisationViewerTransformers.addTransformers(mainView.getVisualizationViewer(), graphModel.getCurrentGraph(), graphModel.getMergedVertices());

        // If merged nothing selected
        mainView.getRightSideScreen().setVertexIdLabel("No vertex selected yet.");
        mainView.getRightSideScreen().setVertexAdditionalInfoLabel("Select one vertex for specific information.");
        mainView.getRightSideScreen().getGoDownButton().setVisible(false);
        mainView.getRightSideScreen().getMergeButton().setVisible(false);

        // If parent above, make back button appear
        if(graphModel.getCurrentGraph().getParent() != null)
        {
            mainView.getRightSideScreen().getGoUpButton().setVisible(true);
        }
        else
        {
            mainView.getRightSideScreen().getGoUpButton().setVisible(false);
        }


        mainView.drawGUI(graphModel.getCurrentGraph());
        mainView.getUndoTransformationDifferentViewerJDialog().setVisible(false);
    }

    /**
     * Doesn't change the current graph that is being visualised in the visualisation viewer but undoes the last
     * transformation. It' important to note that if it so happens that the last transformation was performed on the
     * graph parent, then there is a chance that the current one is result of that transformation, hence, if it's undone
     * the current viewer might cease to exist and the user cannot stay on it, if that's the case the dialog box
     * {@link org.ottr.transformations.views.UndoTransformationMergeChildrenJDialog} is called.
     */
    public void undoMergeDontChangeViewer()
    {
        Merge lastMergePeek = (Merge) graphModel.getTransformationHistory().peekAtLastTransformation();
        NestedGraphTree mergedVertex = lastMergePeek.getMergedVertex();

        // If merge combined two or more deep vertices, and the current viewer is on the resulting merged child graph,
        // when the merge is undone the viewer will have several options of which to display, so if that's the case
        // the viewer is forced to the parent, where the original transformation occurred
        if(mergedVertex.getId() == graphModel.getCurrentGraph().getId())
        {
            System.out.println("On resulted merged graph!");
            mainView.getUndoTransformationMergeChildrenJDialog().setVisible(true);
        }
        else
        {
            Merge lastMerge = (Merge) graphModel.getTransformationHistory().popLastTransformation();

            // Unselect all selected vertices so if we go back they're not selected
            mainView.unselectAllVertices();
            // Unselect all selected edges so if we go back they're not selected
            mainView.unselectAllEdges();


            // Get graph from hash code
            NestedGraphTree graphTransformed = graphModel.getAllVertices().get(lastMerge.getHashCodeOfTreeObjTransformationOccurredOn());


            graphTransformed.setChildren(lastMerge.getOldGraphChildren());
            graphTransformed.setEdges(lastMerge.getOldGraphEdges());

            // Fix parent links of children of merged vertex
            for(NestedGraphTree child : graphTransformed.getChildren())
            {
                for(NestedGraphTree childOfChild : child.getChildren())
                {
                    childOfChild.setParent(child);
                }
            }

            mainView.getUndoTransformationDifferentViewerJDialog().setVisible(false);
        }
    }

    /**
     * Destroys the dialog box and cancels.
     */
    public void cancel()
    {
        mainView.getUndoTransformationDifferentViewerJDialog().dispose();
    }
}
