package org.ottr.transformations.controllers;

import edu.uci.ics.jung.algorithms.layout.ISOMLayout;
import edu.uci.ics.jung.algorithms.layout.Layout;
import edu.uci.ics.jung.graph.Graph;
import org.ottr.graph.models.GraphModel;
import org.ottr.graph.models.representation.NestedGraphTree;
import org.ottr.graph.views.MainView;
import org.ottr.graph.views.VisualisationViewerTransformers;
import org.ottr.transformations.models.Merge;

/**
 * Manages the events from the parent graph is last transformation dialog buttons.
 */
public class UndoTransformationMergeChildrenJDialogListener
{
    private final MainView mainView;
    private final GraphModel graphModel;

    /**
     * Class constructor.
     * @param graphModel the model of the application
     * @param mainView the main view of the application
     */
    public UndoTransformationMergeChildrenJDialogListener
            (
                    GraphModel graphModel,
                    MainView mainView
            )
    {
        this.graphModel = graphModel;
        this.mainView = mainView;
    }

    /**
     * Visualises the graph that's nested in the parent of the current graph object and undoes the transformation on it.
     * This method can only be called from the dialog box
     * {@link org.ottr.transformations.views.UndoTransformationMergeChildrenJDialog}, which can only be triggered from
     * the {@link org.ottr.transformations.views.UndoTransformationDifferentViewerJDialog} after a user has clicked the
     * button which undoes the last transformation done but doesn't change the viewer
     * {@link UndoTransformationDifferentViewerJDialogListener#undoMergeDontChangeViewer()}.
     */
    public void changeViewerOneStepBackAndUndo()
    {
        Merge lastMerge = (Merge) graphModel.getTransformationHistory().popLastTransformation();

        // Unselect all selected vertices so if we go back they're not selected
        mainView.unselectAllVertices();
        // Unselect all selected edges so if we go back they're not selected
        mainView.unselectAllEdges();

        // Get graph from hash code
        graphModel.setCurrentGraph(graphModel.getAllVertices().get(lastMerge.getHashCodeOfTreeObjTransformationOccurredOn()));


        graphModel.getCurrentGraph().setChildren(lastMerge.getOldGraphChildren());
        graphModel.getCurrentGraph().setEdges(lastMerge.getOldGraphEdges());

        // Fix parent links of children of merged vertex
        for(NestedGraphTree child : graphModel.getCurrentGraph().getChildren())
        {
            for(NestedGraphTree childOfChild : child.getChildren())
            {
                childOfChild.setParent(child);
            }
        }

        Graph<Integer, String> graphToDraw = graphModel.getCurrentGraph().createGraph();
        // Create graph
        Layout<Integer, String> layout = new ISOMLayout<>(graphToDraw);
        mainView.getVisualizationViewer().setGraphLayout(layout);

        // Add vertex and edge transformers
        VisualisationViewerTransformers.addTransformers(mainView.getVisualizationViewer(), graphModel.getCurrentGraph(), graphModel.getMergedVertices());

        // If merged nothing selected
        mainView.getRightSideScreen().setVertexIdLabel("No vertex selected yet.");
        mainView.getRightSideScreen().setVertexAdditionalInfoLabel("Select one vertex for specific information.");
        mainView.getRightSideScreen().getGoDownButton().setVisible(false);
        mainView.getRightSideScreen().getMergeButton().setVisible(false);

        // If parent above, make back button appear
        if(graphModel.getCurrentGraph().getParent() != null)
        {
            mainView.getRightSideScreen().getGoUpButton().setVisible(true);
        }
        else
        {
            mainView.getRightSideScreen().getGoUpButton().setVisible(false);
        }

        mainView.drawGUI(graphModel.getCurrentGraph());
        mainView.getUndoTransformationDifferentViewerJDialog().setVisible(false);
        mainView.getUndoTransformationMergeChildrenJDialog().setVisible(false);
    }

    /**
     * Destroys the dialog box and cancels.
     */
    public void cancel()
    {
        mainView.getUndoTransformationMergeChildrenJDialog().dispose();
    }
}
