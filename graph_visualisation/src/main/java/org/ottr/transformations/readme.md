# Transformation package

All things about transformations including types of transformations, events, transformation
history, etc.

Visual elements are included as well, like transformation popups.