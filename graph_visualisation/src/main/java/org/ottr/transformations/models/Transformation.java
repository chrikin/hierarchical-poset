package org.ottr.transformations.models;

import java.io.Serial;
import java.io.Serializable;

/**
 * Represents a general transformation.
 */
public abstract class Transformation implements Serializable
{
    @Serial
    private static final long serialVersionUID = 0;
    transformationType type;
    int idOfGraphTransformationOccurredOn;
    int hashCodeOfTreeObjTransformationOccurredOn;

    public Transformation(){}

    /**
     * Getter for the type of transformation, must be specified.
     * @return type of transformation
     */
    public abstract transformationType getType();

    /**
     * Getter for the id of the graph the transformation was performed on.
     * @return id of the graph the transformation was performed on
     */
    public abstract int getIdOfGraphTransformationOccurredOn();

    /**
     * Getter for the hash code of the object the transformation was performed on.
     * @return hash code of the object the transformation was performed on
     */
    public abstract int getHashCodeOfTreeObjTransformationOccurredOn();

    public enum transformationType implements Serializable
    {
        MERGE
    }
}
