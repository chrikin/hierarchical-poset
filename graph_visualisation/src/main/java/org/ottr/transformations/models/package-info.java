/**
 * Representing transformations including contents and history.
 */
package org.ottr.transformations.models;