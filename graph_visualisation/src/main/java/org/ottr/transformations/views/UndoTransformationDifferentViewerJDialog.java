package org.ottr.transformations.views;

import org.ottr.transformations.controllers.UndoTransformationDifferentViewerJDialogListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Dialog for undoing transformation from a different viewer.
 */
public class UndoTransformationDifferentViewerJDialog extends JDialog
{
    private final JButton goAndUndoButton;
    private final JButton undoDontGoButton;
    private final JButton cancelButton;
    private final Font buttonFont;

    private final String[] options =
            {
                    "Go to it and undo",
                    "Undo, but don't go",
                    "Cancel"
            };

    /**
     * Class constructor.
     * @param frame the frame above which the dialog box centers
     */
    public UndoTransformationDifferentViewerJDialog
            (
                    JFrame frame
            )
    {
        goAndUndoButton = new JButton(options[0]);
        undoDontGoButton = new JButton(options[1]);
        cancelButton = new JButton(options[2]);
        buttonFont = new Font("SansSerif", Font.BOLD, 12);

        setTitle("Warning!");
        setLayout(new BorderLayout());
        setSize(600,320);
        setLocationRelativeTo(frame);

        JPanel text_box = new JPanel();
        text_box.setLayout(new FlowLayout());

        //TODO: vectorised image
        ImageIcon question_image = new ImageIcon("../graph_visualisation/resources/dialog_pics/question_image.png");

        text_box.setBorder(new EmptyBorder(20, 0, 10, 0));

        JLabel text_diff_viewer = new JLabel
                (
                        "<html>" +
                                "The transformation you're trying to undo was done <br> on a different graph, do you want to go to that viewer?" +
                                "</html>",
                        question_image,
                        SwingConstants.RIGHT
                );
        text_diff_viewer.setFont(new Font("SansSerif", Font.PLAIN, 15));

        text_box.add(text_diff_viewer);

        // Button box
        JPanel button_box = new JPanel(new GridLayout(1,3));
        button_box.setBorder(new EmptyBorder(20, 0, 0, 0));

        JPanel go_and_undo_box = new JPanel();
        JPanel undo_dont_go_box = new JPanel();
        JPanel cancel_box = new JPanel();

        // Go and undo
        goAndUndoButton.setPreferredSize(new Dimension(150, 30));
        goAndUndoButton.setFont(buttonFont);
        goAndUndoButton.setBackground(Color.decode("#D3D3D3"));

        go_and_undo_box.add(goAndUndoButton);
        go_and_undo_box.setBorder(new EmptyBorder(0, 40, 20, 10));

        // Undo, but don't go
        undoDontGoButton.setPreferredSize(new Dimension(150, 30));
        undoDontGoButton.setFont(buttonFont);
        undoDontGoButton.setBackground(Color.decode("#D3D3D3"));

        undo_dont_go_box.add(undoDontGoButton);
        undo_dont_go_box.setBorder(new EmptyBorder(0, 10, 20, 10));

        // Cancel
        cancelButton.setPreferredSize(new Dimension(150, 30));
        cancelButton.setFont(buttonFont);
        cancelButton.setBackground(Color.decode("#D3D3D3"));

        cancel_box.add(cancelButton);
        cancel_box.setBorder(new EmptyBorder(0, 10, 20, 40));


        button_box.add(go_and_undo_box);
        button_box.add(undo_dont_go_box);
        button_box.add(cancel_box);


        add(text_box, BorderLayout.CENTER);
        add(button_box, BorderLayout.PAGE_END);

        setVisible(false);
    }

    /**
     * Getter for the button responsible for changing the graph on the visualisation viewer and undoing the last
     * transformation, with action listener
     * {@link UndoTransformationDifferentViewerJDialogListener#changeViewerAndUndoMerge()}.
     * @return button for changing the graph on the visualisation viewer and undoing the last transformation
     */
    public JButton getGoAndUndoButton()
    {
        return goAndUndoButton;
    }

    /**
     * Getter for the button responsible for not changing the graph on the visualisation viewer, but undoing the last
     * transformation, with action listener
     * {@link UndoTransformationDifferentViewerJDialogListener#undoMergeDontChangeViewer()}.
     * @return button for not changing the graph on the visualisation viewer, but undoing the last transformation
     */
    public JButton getUndoDontGoButton()
    {
        return undoDontGoButton;
    }

    /**
     * Getter for the cancel button for the dialog box.
     * @return the cancel button
     */
    public JButton getCancelButton()
    {
        return cancelButton;
    }
}
