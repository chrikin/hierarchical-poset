package org.ottr.transformations.views;

import org.ottr.transformations.controllers.UndoTransformationDifferentViewerJDialogListener;
import org.ottr.transformations.controllers.UndoTransformationMergeChildrenJDialogListener;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * Dialog for undoing transformation that's on the parent graph.
 */
public class UndoTransformationMergeChildrenJDialog extends JDialog
{
    private final JButton goBackOnceAndUndoButton;
    private final JButton cancelButton;
    private final Font buttonFont;
    private final String[] options =
            {
                    "Go back and undo",
                    "Cancel"
            };

    /**
     * Class constructor.
     * @param frame the frame above which the dialog box centers
     */
    public UndoTransformationMergeChildrenJDialog
            (
                    JFrame frame
            )
    {
        goBackOnceAndUndoButton = new JButton(options[0]);
        cancelButton = new JButton(options[1]);
        buttonFont = new Font("SansSerif", Font.BOLD, 12);

        setTitle("Warning!");
        setLayout(new BorderLayout());
        setSize(600,320);
        setLocationRelativeTo(frame);

        JPanel text_box = new JPanel();
        text_box.setLayout(new FlowLayout());

        text_box.setBorder(new EmptyBorder(90, 0, 10, 0));

        JLabel text_diff_viewer = new JLabel
                (
                        "<html>" +
                                "<p style=\"text-align:center\"> Current graph will split if last transformation is undone, <br> go back to parent? </p>" +
                                "</html>"
                );
        text_diff_viewer.setFont(new Font("SansSerif", Font.PLAIN, 15));

        text_box.add(text_diff_viewer);

        // Button box
        JPanel button_box = new JPanel(new GridLayout(1,3));
        button_box.setBorder(new EmptyBorder(20, 0, 0, 0));

        JPanel goBackOnceAndUndoBox = new JPanel();
        JPanel cancel_box = new JPanel();

        // Go and undo
        goBackOnceAndUndoButton.setPreferredSize(new Dimension(150, 30));
        goBackOnceAndUndoButton.setFont(buttonFont);
        goBackOnceAndUndoButton.setBackground(Color.decode("#D3D3D3"));

        goBackOnceAndUndoBox.add(goBackOnceAndUndoButton);
        goBackOnceAndUndoBox.setBorder(new EmptyBorder(0, 40, 20, 10));

        // Cancel
        cancelButton.setPreferredSize(new Dimension(150, 30));
        cancelButton.setFont(buttonFont);
        cancelButton.setBackground(Color.decode("#D3D3D3"));

        cancel_box.add(cancelButton);
        cancel_box.setBorder(new EmptyBorder(0, 10, 20, 40));


        button_box.add(goBackOnceAndUndoBox);
        button_box.add(cancel_box);


        add(text_box, BorderLayout.CENTER);
        add(button_box, BorderLayout.PAGE_END);

        setVisible(false);
    }

    /**
     * Getter for the button responsible for changing the graph on the visualisation viewer one level up (so it brings
     * the visualisation viewer to the parent graph) and undoing the last transformation, with action listener
     * {@link UndoTransformationMergeChildrenJDialogListener#changeViewerOneStepBackAndUndo()}.
     * @return button for changing the graph on the visualisation viewer to parent graph and undoing
     * the last transformation
     */
    public JButton getGoBackOnceAndUndoButton()
    {
        return goBackOnceAndUndoButton;
    }

    /**
     * Getter for the cancel button for the dialog box.
     * @return the cancel button
     */
    public JButton getCancelButton()
    {
        return cancelButton;
    }
}
