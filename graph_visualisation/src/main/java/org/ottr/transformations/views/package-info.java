/**
 * Visualising transformations and dialog boxes associated with them.
 */
package org.ottr.transformations.views;