package org.ottr;

import org.ottr.graph.models.GraphModel;
import org.ottr.graph.controllers.GraphController;
import org.ottr.graph.models.representation.NestedGraphTree;
import org.ottr.graph.views.MainView;

/**
 * Driver class for the application.
 */
public class App
{
    // Path to JSON with graph information at default path
    static String defaultPath = "../graph_visualisation/data/graph.json";
    // Path to save dir where graphs are saved with file menu option
    static String savePath = "../graph_visualisation/target/output.txt";

    /**
     * Main method for the project.
     */
    public static void main(String[] args) throws Exception
    {
        // Graph info extracted from JSON
        NestedGraphTree tree = DataLoader.initFromJSON(getDefaultPath());

        // Initiate model
        GraphModel graphModel = new GraphModel
        (
            tree
        );

        // Initiate view
        MainView mainView = new MainView();

        GraphController controller = new GraphController(graphModel, mainView);
        controller.initController();
    }

    /**
     * Stores default path from which the graph.json is taken.
     * @return default path for input
     */
    public static String getDefaultPath()
    {
        return defaultPath;
    }

    /**
     * Stores save path for saving state of project.
     * @return save path for output
     */
    public static String getSavePath()
    {
        return savePath;
    }
}
